Probability Experiments
=======================

An attempt to explore some (often counterintuitive) concepts in probability, with the hope that simulating them would improve my understanding of them.


Birthday Paradox
----------------
I've heard a number of times about the phenomenon that two people in a group of people will have the same birthday more often than intuition would suggest. Apparently only 23 people are required to achieve a 50% probability of two people having the same birthday.

The probability of a particular person (e.g., me) having the same birthday as someone else is much smaller.


Potential Future Experiments
----------------------------

Monty Hall problem--where did I run into this? Irrational Exuberance? Maybe something by Dan Ariely? (Need to check Predictably Irrational and Dollars and Sense.)